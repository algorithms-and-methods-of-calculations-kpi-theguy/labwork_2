package com.kpi.yuriy.labwork2.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kpi.yuriy.labwork2.HeapSort;
import com.kpi.yuriy.labwork2.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.app.Activity.RESULT_OK;


public class sortArray extends Fragment {

    private LinearLayout linear_but;
    private Button button_file;
    private Button button_1;
    private Button button_2;

    private LinearLayout linear_load;
    private Button button_search2;
    private EditText editText_file2;
    private Button button_load;

    private GridLayout sorted_table;
    private LinearLayout[][] cells;

    private final int PICKRESULT_OK = 1;
    private HeapSort hs;

    private OnFragmentInteractionListener mListener;

    public sortArray() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sort_array, container, false);

        linear_but = (LinearLayout) v.findViewById(R.id.linear_but);
        button_file = (Button) v.findViewById(R.id.button_file);
        button_1 = (Button) v.findViewById(R.id.button_1);
        button_2 = (Button) v.findViewById(R.id.button_2);
        linear_load = (LinearLayout) v.findViewById(R.id.linear_load);
        button_search2 = (Button) v.findViewById(R.id.button_search2);
        editText_file2 = (EditText) v.findViewById(R.id.editText_file3);
        button_load = (Button) v.findViewById(R.id.button_load);

        sorted_table = (GridLayout) v.findViewById(R.id.sorted_table);

        hs = new HeapSort();

        button_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_load.setVisibility(View.VISIBLE);
                linear_but.setVisibility(View.GONE);
            }
        });

        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = getArguments();
                if (args == null){
                    Toast.makeText(getActivity(), "Wrong argument", Toast.LENGTH_SHORT).show();
                }
                else{
                    String str = args.getString("addArrays");
                    makeTable(str);
                }
            }
        });

        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = getArguments();
                if (args == null){
                    Toast.makeText(getActivity(), "Wrong argument", Toast.LENGTH_SHORT).show();
                }
                else{
                    String str = args.getString("addArrayss");
                    makeTable(str);
                }
            }
        });

        button_search2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKRESULT_OK);
            }
        });
        button_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri buffUri = Uri.parse(editText_file2.getText().toString());
                String str = "";
                String line = "";
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(buffUri);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    while(null != (line = br.readLine())){
                        str = line;
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                makeTable(str);
            }
        });
        return v;
    }

    public void generateTable (double[] array){
        sorted_table.removeAllViews();
        int N = array.length;
        int numCells = (int) Math.ceil((double)N/4);
        sorted_table.setColumnCount(4);
        sorted_table.setRowCount(numCells);
        cells = new LinearLayout[numCells][4];
        for (int i = 0; i < numCells; i++){
            for (int j = 0; j < 4 && (i*4 + j) < N; j++){
                generateCell(i, j, array[i*4 + j]);
            }
        }
    }

    public void generateCell (int row, int col, double content){
        cells[row][col] = new LinearLayout(getActivity());
        cells[row][col].setOrientation(LinearLayout.VERTICAL);
        TextView text = new TextView(getActivity());
        text.setText(String.valueOf(4*row + col));
        text.setTextColor(Color.BLACK);
        text.setLayoutParams(new LinearLayout.LayoutParams(250,50));
        text.setGravity(Gravity.CENTER);
        EditText mainText = new EditText(getActivity());
        mainText.setLayoutParams(new LinearLayout.LayoutParams(250,150));
        mainText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mainText.setTextColor(Color.BLACK);
        mainText.setEnabled(false);
        mainText.setText(Double.toString(content));
        cells[row][col].addView(text);
        cells[row][col].addView(mainText);
        cells[row][col].setLayoutParams(new LinearLayout.LayoutParams(250,200));
        sorted_table.addView(cells[row][col]);
    }

    public void makeTable (String str){
        String[] buffStr = str.split(" ");
        double[] arrays = new double[buffStr.length];
        for (int i = 0; i < buffStr.length; i++){
            arrays[i] = Double.parseDouble(buffStr[i]);
        }
        mListener.onFragmentSort(arrays);
        hs.sort(arrays);
        generateTable(arrays);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKRESULT_OK) {
                Uri uri = data.getData();
                editText_file2.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentSort(double[] d);
    }
}
