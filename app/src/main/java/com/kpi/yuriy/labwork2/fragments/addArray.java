package com.kpi.yuriy.labwork2.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kpi.yuriy.labwork2.R;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class addArray extends Fragment {

    private EditText editText_N;
    private Button button_submit;
    private Button button_generate;

    private GridLayout table;
    private LinearLayout[][] cells;
    private EditText[] edits;
    private Random rand = new Random(System.currentTimeMillis());

    private LinearLayout linear_save;
    private Button button_search;
    private EditText editText_file;
    private Button button_add;

    private StringBuilder globarStr = new StringBuilder();
    private final int PICKRESULT_OK = 1;

    private OnFragmentInteractionListener mListener;

    public addArray() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_array, container, false);

        editText_N = (EditText) v.findViewById(R.id.editText_N);
        button_submit = (Button) v.findViewById(R.id.button_submit);
        button_generate = (Button) v.findViewById(R.id.button_generate);
        table = (GridLayout) v.findViewById(R.id.table);
        linear_save = (LinearLayout) v.findViewById(R.id.linear_save);
        button_search = (Button) v.findViewById(R.id.button_search);
        editText_file = (EditText) v.findViewById(R.id.editText_file3);
        button_add = (Button) v.findViewById(R.id.button_add);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_save.setVisibility(View.VISIBLE);
                int tempN = Integer.parseInt(editText_N.getText().toString());
                generateTable(tempN, false);
            }
        });
        button_generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_save.setVisibility(View.VISIBLE);
                int tempN = Integer.parseInt(editText_N.getText().toString());
                generateTable(tempN, true);
            }
        });
        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKRESULT_OK);
            }
        });
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText_file.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "File haven't entered!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Uri buff = Uri.parse(editText_file.getText().toString());
                    try {
                        OutputStream os = getActivity().getContentResolver().openOutputStream(buff);
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
                        bw.write("");
                        StringBuilder str = new StringBuilder();
                        for (int i = 0; i < edits.length; i++) {
                            str.append(edits[i].getText().toString()).append(" ");
                        }
                        mListener.onFragmentaddArray(str.toString());
                        globarStr.append(str).append("\n");
                        bw.append(globarStr);
                        bw.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void generateTable (int N, boolean flag){
        table.removeAllViews();
        int numCells = (int) Math.ceil((double)N/4);
        edits = new EditText[N];
        table.setColumnCount(4);
        table.setRowCount(numCells);
        cells = new LinearLayout[numCells][4];
        for (int i = 0; i < numCells; i++){
            for (int j = 0; j < 4 && (i*4 + j) < N; j++){
                generateCell(i, j, flag);
            }
        }
    }

    public void generateCell (int row, int col, boolean flag){
        cells[row][col] = new LinearLayout(getActivity());
        cells[row][col].setOrientation(LinearLayout.VERTICAL);
        TextView text = new TextView(getActivity());
        text.setText(String.valueOf(4*row + col));
        text.setLayoutParams(new LinearLayout.LayoutParams(250,50));
        text.setGravity(Gravity.CENTER);
        edits[4*row+col] = new EditText(getActivity());
        edits[4*row+col].setLayoutParams(new LinearLayout.LayoutParams(250,150));
        edits[4*row+col].setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        if (flag){
            double randm = 50000;
            edits[4*row+col].setText(Double.toString(((rand.nextDouble()*2)-1)*randm));
        }
        cells[row][col].addView(text);
        cells[row][col].addView(edits[4*row+col]);
        cells[row][col].setLayoutParams(new LinearLayout.LayoutParams(250,200));
        table.addView(cells[row][col]);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKRESULT_OK) {
                Uri uri = data.getData();
                editText_file.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
            }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentaddArray(String s);
    }
}
