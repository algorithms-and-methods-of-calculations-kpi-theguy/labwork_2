package com.kpi.yuriy.labwork2.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.kpi.yuriy.labwork2.HeapSort;
import com.kpi.yuriy.labwork2.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class Graphics extends Fragment {

    private LinearLayout linear_graph;

    private Button button_search3;
    private EditText editText_file3;
    private Button button_graph;

    private GraphView graphic;
    private DataPoint[] first_graph;
    private DataPoint[] second_graph;
    private double[] times;
    private double[] arrays;

    private ProgressDialog pd;

    private final int PICKRESULT_OK = 1;
    private HeapSort hs;

    private int numArrays;

    public Graphics() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_graphics, container, false);

        linear_graph = (LinearLayout) v.findViewById(R.id.linear_graph);
        button_search3 = (Button) v.findViewById(R.id.button_search3);
        editText_file3 = (EditText) v.findViewById(R.id.editText_file3);
        button_graph = (Button) v.findViewById(R.id.button_graph);
        graphic = (GraphView) v.findViewById(R.id.graphic);
        hs = new HeapSort();

        first_graph = new DataPoint[numArrays];

        button_search3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKRESULT_OK);
            }
        });

        button_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear_graph.setVisibility(View.GONE);
                Uri buffUri = Uri.parse(editText_file3.getText().toString());
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(buffUri);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String str = "";
                    ArrayList<String> buffArr = new ArrayList<>();
                    while ((str = br.readLine()) != null){
                        buffArr.add(str);
                    }
                    numArrays = buffArr.size();
                    sortArrays(buffArr);
                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(first_graph);
                    series.setDrawBackground(true);
                    series.setBackgroundColor(Color.CYAN);
                    series.setTitle("arrLength(time)");
                    series.setColor(Color.CYAN);
                    LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(second_graph);
                    series2.setTitle("O(n*log(n)");
                    series2.setColor(Color.RED);
                    series2.setThickness(5);
                    graphic.addSeries(series);
                    graphic.addSeries(series2);
                    graphic.getLegendRenderer().setVisible(true);
                    graphic.getLegendRenderer().setBackgroundColor(Color.argb(0x80, 0x80, 0x80, 0xff));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKRESULT_OK) {
                Uri uri = data.getData();
                editText_file3.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sortArrays (ArrayList<String> al){
        first_graph = new DataPoint[numArrays];
        second_graph = new DataPoint[numArrays];
        arrays = new double[numArrays];
        times = new double[numArrays];
        for (int i = 0; i < numArrays; i++) {
            String buff[] = al.get(i).split(" ");
            double arr[] = new double[buff.length];
            for (int j = 0; j < buff.length; j++) {
                arr[j] = Double.parseDouble(buff[j]);
            }
            arrays[i] = arr.length;
            double startPoint = System.nanoTime()/1000;
            hs.sort(arr);
            double endPoint = System.nanoTime()/1000;
            times[i] = endPoint - startPoint;
            first_graph[i] = new DataPoint(arrays[i], times[i]);
        }
        for (int i = 0; i < numArrays; i++){
            second_graph[i] = new DataPoint(arrays[i], 20*(i*Math.log(i)));
        }
    }

}
