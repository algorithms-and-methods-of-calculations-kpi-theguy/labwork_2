package com.kpi.yuriy.labwork2.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kpi.yuriy.labwork2.R;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class addArrays extends Fragment {

    private SeekBar seekBar_array;
    private SeekBar seekBar_minNumber;
    private SeekBar seekBar_step;

    private int numArrays;
    private int minNumberArr;
    private int stepBetween;

    private TextView textView_1;
    private TextView textView_2;
    private TextView textView_3;

    private Button button_search;
    private EditText editText_file;
    private Button button_generate;

    private Random rand = new Random(System.currentTimeMillis());

    private ExtendedGeneration extG;
    private ProgressDialog pd;

    private OnFragmentInteractionListener mListener;

    private final int PICKRESULT_OK = 1;

    public addArrays() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_arrays, container, false);

        seekBar_array = (SeekBar) v.findViewById(R.id.seekBar_array);
        textView_1 = (TextView) v.findViewById(R.id.textView_1);
        seekBar_minNumber = (SeekBar) v.findViewById(R.id.seekBar_minNumber);
        textView_2 = (TextView) v.findViewById(R.id.textView_2);
        seekBar_step = (SeekBar) v.findViewById(R.id.seekBar_step);
        textView_3 = (TextView) v.findViewById(R.id.textView_3);
        button_search = (Button) v.findViewById(R.id.button_search1);
        editText_file = (EditText) v.findViewById(R.id.editText_file3);
        button_generate = (Button) v.findViewById(R.id.button_generate);

        seekBar_array.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView_1.setText(String.valueOf(progress));
                numArrays = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar_minNumber.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView_2.setText(String.valueOf(progress));
                minNumberArr = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar_step.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView_3.setText(String.valueOf(progress));
                stepBetween = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("plain/text");
                startActivityForResult(intent, PICKRESULT_OK);
            }
        });
        button_generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText_file.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "File haven't entered!", Toast.LENGTH_SHORT).show();
                }
                else {
                    extG = new ExtendedGeneration();
                    Uri uri = Uri.parse(editText_file.getText().toString());
                    try {
                        OutputStream os = getActivity().getContentResolver().openOutputStream(uri);
                        BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(os));
                        extG.execute(bw1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKRESULT_OK) {
                Uri uri = data.getData();
                editText_file.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    class ExtendedGeneration extends AsyncTask<BufferedWriter, Integer, Void>{

        @Override
        protected Void doInBackground(BufferedWriter... bufferedWriters) {
            for(BufferedWriter bw: bufferedWriters){
                try {
                    bw.write("");
                    StringBuilder str;
                    StringBuilder res = new StringBuilder();
                    double randm = 50000;
                    int counter = 0;
                    for (int i = 0; i < numArrays; i++) {
                        str = new StringBuilder();
                        for (int j = 0; j < minNumberArr + counter; j++) {
                            str.append(Double.toString(((rand.nextDouble() * 2) - 1) * randm)).append(" ");
                            mListener.onFragmentaddArrays(str.toString());
                        }
                        counter += stepBetween;
                        res.append(str).append("\n");
                        publishProgress(i);
                    }
                    bw.append(res);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute(){
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Generating arrays");
            pd.setMessage("Wait for seconds");
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setProgress(0);
            pd.setMax(numArrays);
            pd.show();
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pd.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            Toast.makeText(getActivity(), "Arrays generated!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentaddArrays(String s);
    }

}
